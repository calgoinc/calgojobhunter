package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.entity.TeacherCord;

public interface CordRepository extends JpaRepository <TeacherCord,String>{
	@Query(value = "SELECT TEACHERCORD FROM TEACHERCORD", nativeQuery = true)
	String findTeachercord();
	
	@Query(value = "SELECT STDCORD FROM STUDENTCORD", nativeQuery = true)
	String findStdcord();

}
