package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.entity.Account;

public interface AccountRepository extends JpaRepository<Account, String> {
	@Query("FROM Account a WHERE a.userid = :userid")
	Account findByUsername(@Param("userid") String userid);
	
	String userName = null;

	@Query(value = "SELECT STD_EM FROM ACCOUNTS WHERE USERID = ?", nativeQuery = true)
	String judgmentstudent(String userName);
	
	int countByUserid(String userid);
	
}
