package com.example.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.entity.Account;
import com.example.repository.AccountRepository;
import com.example.repository.CordRepository;
import com.example.service.RegisterAccountService;

@Controller
public class AccountController {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	CordRepository cordRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	RegisterAccountService registerAccountService;

	@RequestMapping(value = "/signupmain", method = RequestMethod.GET)
	public ModelAndView add(@ModelAttribute("userRequset") Account account, ModelAndView mav) {
		mav.setViewName("login/signupmain");
		return mav;
	}

	@RequestMapping(value = "/signupsub1", method = RequestMethod.POST) // アカウント登録時などの表示文
	@Transactional(readOnly = false)
	public ModelAndView form(@ModelAttribute("userRequest") Account account, ModelAndView mav, String mailaddress,
			String name, String gakka, String gakunen, String kurasu, String bangou) {
		if (accountRepository.countByUserid(account.getUserid()) > 0) {
			mav.addObject("obj", account.getUserid() + "は既に使用されてるユーザ名です。");
			mav.setViewName("login/signupsub1");
		} else {
			account.setPassword(passwordEncoder.encode(account.getPassword()));
			account.setMailaddress(mailaddress);
			account.setName(name);
			account.setGakka(gakka);
			account.setKurasu(kurasu);
			account.setGakunen(gakunen);
			account.setBangou(bangou);
			account.setTeacher("0");
			account.setStd_em("0");
			accountRepository.saveAndFlush(account);
			mav.setViewName("login/signupconf");
		}
		return mav;
	}
	
	@RequestMapping(value = "/signupsub2", method = RequestMethod.POST) // アカウント登録時などの表示文
	@Transactional(readOnly = false)
	public ModelAndView forms(@ModelAttribute("userRequest") Account account, ModelAndView mav, String mailaddress,
			String name, String gakka, String gakunen, String kurasu, String bangou) {
		if (accountRepository.countByUserid(account.getUserid()) > 0) {
			mav.addObject("obj", account.getUserid() + "は既に使用されてるユーザ名です。");
			mav.setViewName("login/signupsub2");
		} else {
			account.setPassword(passwordEncoder.encode(account.getPassword()));
			account.setMailaddress(mailaddress);
			account.setName(name);
			account.setGakka(gakka);
			account.setKurasu(kurasu);
			account.setGakunen(gakunen);
			account.setBangou(bangou);
			account.setTeacher("0");
			account.setStd_em("1");
			accountRepository.saveAndFlush(account);
			mav.setViewName("login/signupconf");
		}
		return mav;
	}

	@RequestMapping(value = "/signupsub3", method = RequestMethod.POST) // アカウント登録時などの表示文
	@Transactional(readOnly = false)
	public ModelAndView form(@ModelAttribute("userRequest") Account account, ModelAndView mav, String mailaddress,
			String name) {
		if (accountRepository.countByUserid(account.getUserid()) > 0) {
			mav.addObject("obj", account.getUserid() + "は既に使用されてるユーザ名です。");
			mav.setViewName("login/signupsub3");
		} else {
			account.setPassword(passwordEncoder.encode(account.getPassword()));
			account.setMailaddress(mailaddress);
			account.setName(name);
			account.setGakka("/");
			account.setKurasu("/");
			account.setGakunen("/");
			account.setBangou("/");
			account.setTeacher("1");
			account.setStd_em("0");
			accountRepository.saveAndFlush(account);
			mav.setViewName("login/signupconf");
		}
		return mav;
	}

	@RequestMapping(value = "/teacherconf", method = RequestMethod.POST)
	public String teachcord(@ModelAttribute("teachercord") String tcord) {
		String pass = cordRepository.findTeachercord();
		if (tcord.equals(pass)) {
			return "login/signupsub3";
		}
		return "login/teacherconf";
	}
	
	
	 @RequestMapping(value = "/studentconf", method = RequestMethod.POST)
	public String stucord(@ModelAttribute("stdcord") String stdcord) {
		String stucord = cordRepository.findStdcord();
		if (stdcord.equals(stucord)) {
			return "login/signupsub2";
		}
		return "login/employmentconf";
	}

}
