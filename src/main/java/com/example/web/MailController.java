package com.example.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class MailController {
	private final JavaMailSender javaMailSender;
    @Autowired
    MailController(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @RequestMapping(value = "/mailconf", method = {RequestMethod.POST} )
    public ModelAndView send(@ModelAttribute("mailaddress") String mail) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(mail);
        mailMessage.setFrom("calgo.jp@gmail.com");
        mailMessage.setSubject("会員登録ページ");
        mailMessage.setText("http://localhost:8080/signupmain");
        javaMailSender.send(mailMessage);
        return new ModelAndView("login/mailconf");
    }

}
