package com.example.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

	@GetMapping("/")
	String login() {
		return "login/login";
	}

	@GetMapping("/mailconf")
	String mailconf() {
		return "login/mailconf";
	}

	@GetMapping("/signupsub1")
	String signupsub1() {
		return "login/signupsub1";
	}
	
	@GetMapping("/signupsub2")
	String signupsub2() {
		return "login/signupsub2";
	}
	
	@GetMapping("/signupsub3")
	String signupsub3() {
		return "login/signupsub3";
	}

	@GetMapping("/signupconf")
	String signupconf() {
		return "login/signupconf";
	}
	
	@GetMapping("/teacherconf")
	String teacherconf() {
		return "login/teacherconf";
	}
	
	@GetMapping("/studentconf")
	String studentconf() {
		return "login/studentconf";
	}

}
