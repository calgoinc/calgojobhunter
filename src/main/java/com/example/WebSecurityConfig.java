package com.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@Order(2)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/image/**", "/font/**", "/css/**", "/js/**", "/webjars/**", "/console/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/", "/teacherconf", "/studentconf", "/mailconf", "/signupmain", "/signupsub1",
						"/signupsub2", "/signupsub3", "/signupconf", "/menu1", "/menu2", "/menu3", "/p1create",
						"/p1delete", "/p1edit", "/p1main", "/p2create", "/p2delete", "/p2edit", "/p2main","/p3main","/p3result",
						"/p4main","/p4fail","/p4success","p5main","p5fail","p5success")
				.permitAll().anyRequest().authenticated().and().formLogin().loginProcessingUrl("/login").loginPage("/")// ログインページ
				.usernameParameter("userid")// パラメータ
				.passwordParameter("password")// パラメータ
				.failureUrl("/?error").defaultSuccessUrl("/mailconf") // 認証成功時の遷移先
				.permitAll().and().logout().permitAll();
		http.csrf().disable();
	}

	// PasswordEncoder
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
