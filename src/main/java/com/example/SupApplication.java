package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupApplication {

	public static void main(String[] args) {
		SpringApplication.run(SupApplication.class, args);
	}

}
