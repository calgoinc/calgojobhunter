package com.example.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "accounts")
public class Account {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;
	@Id
	private String userid;
	@JsonIgnore
	private String password;
	private String mailaddress;
	private String name;
	private String gakka;
	private String gakunen;
	private String kurasu;
	private String bangou;
	private String teacher;
	private String std_em;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMailaddress() {
		return mailaddress;
	}

	public void setMailaddress(String mailaddress) {
		this.mailaddress = mailaddress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGakka() {
		return gakka;
	}

	public void setGakka(String gakka) {
		this.gakka = gakka;
	}

	public String getGakunen() {
		return gakunen;
	}

	public void setGakunen(String gakunen) {
		this.gakunen = gakunen;
	}

	public String getKurasu() {
		return kurasu;
	}

	public void setKurasu(String kurasu) {
		this.kurasu = kurasu;
	}

	public String getBangou() {
		return bangou;
	}

	public void setBangou(String bangou) {
		this.bangou = bangou;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public String getStd_em() {
		return std_em;
	}

	public void setStd_em(String std_em) {
		this.std_em = std_em;
	}

	/*
	 * alter table accounts add id char(100); alter table accounts add username
	 * varchar(100); alter table accounts add password varchar(255); alter table
	 * accounts add email varchar(100); alter table accounts add fullname
	 * varchar(100); alter table accounts add subject varchar(100); alter table
	 * accounts add kurasu varchar(100); alter table accounts add number
	 * varchar(100); alter table accounts add teacher varchar(100); alter table
	 * accounts add employment varchar(100);
	 */
	/*
	 * create table accounts(id varchar(100) AUTO_INCREMENT, userid varchar(255) NOT
	   NULL PRIMARY KEY, password varchar(255), mailaddress varchar(255), name
	   varchar(100), gakka varchar(10), kurasu varchar(10), gakunen varchar(10),
	   bangou varchar(10), teacher varchar(10), std_em varchar(10));
	 */
}
