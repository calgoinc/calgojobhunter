package com.example.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "studentcord")
public class StudentCord {
	@Id
	private String stdcord;

	public String getStdcord() {
		return stdcord;
	}

	public void setStdcord(String stdcord) {
		this.stdcord = stdcord;
	}
	
	//create table studentcord(stdcord varchar(255));
	
	

}
