package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.repository.CordRepository;

public class CordService {
	
	@Autowired
	CordRepository teacherRepository;
	
	public String findTeachercord(){//教員コード
		return teacherRepository.findTeachercord();
	}
	
	public String findStdcord(){//就職学年コード
		return teacherRepository.findStdcord();
	}

}
